---
title: "Git-ohjeita"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---

# git-versiohallinta

Mikäli A) käytät R:ää tai muuta *skriptattavaa* (vs. valikkopohjainen) ohjelmaa datan analysoimisessa, ja kirjoitat B) analyysit tekstimuotoisiin (plain text) tiedostoihin kuten Rmarkdown, on versiohallinnan (revision control / version control) käyttöönotto on erinomainen idea.

**Miksi?**

- oman työn ja tiedonhallinnan "kestävyys" (sustainable software and data management practises)
- oppiminen ja kontribuoiminen tutkimusprojekteihin ja tutkimusohjelmistoihin (git + github/gitlab on tällä hetkellä se infrastruktuuri, jossa kehitystyö pitkälti tapahtuu)
- tieteen avoimuus ja toistettavuus (reproducibility)
    - datat & analyysit (julkaisut jo on osin avoimia ja asia on ns. prosessissa)
    - analyysien (ohjelmistojen) tekeminen viitattaviksi
    - datojen dokumentointi, lisensointi ja esillepano



# Oppaita

- <https://www.youtube.com/watch?v=U8GBXvdmHT4> - aloita tästä 54 minuuttia
- <https://try.github.io/levels/1/challenges/1> sitten tämä - 15 minuttia
- <https://guides.github.com/>
- <https://www.youtube.com/githubguides>
- <https://git-scm.com/book/fi/v1/Alkusanat-Versionhallinnasta>
* <http://happygitwithr.com>
* <https://beta.rstudioconnect.com/jennybc/happy-git-with-r/>
- [Greg Wilson, Jennifer Bryan, Karen Cranston, Justin Kitzes, Lex Nederbragt, Tracy K. Teal (2016) Good Enough Practices in Scientific Computing](https://arxiv.org/pdf/1609.00037v1.pdf)
- [TIE-02200 Ohjelmoinnin peruskurssi S2015: Git ja versionhallinta](http://www.cs.tut.fi/~opersk/S2015/@wrapper.shtml?materiaalit/git)


<iframe width="560" height="315" src="https://www.youtube.com/embed/qv_UKAoCwN4" frameborder="0" allowfullscreen></iframe>
